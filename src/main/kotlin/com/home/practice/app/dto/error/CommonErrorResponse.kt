package com.home.practice.app.dto.error

data class CommonErrorResponse(
        val message: String
)
