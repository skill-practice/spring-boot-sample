package com.home.practice.app.dto.http.account

data class UpdateAccountRequestDto(
        val username: String,
        val email: String,
        val firstName: String,
        val middleName: String?,
        val lastName: String,
)
