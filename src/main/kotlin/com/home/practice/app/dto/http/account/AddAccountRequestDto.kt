package com.home.practice.app.dto.http.account

data class AddAccountRequestDto(
        val username: String,
        val email: String,
        val password: String,
        val firstName: String,
        val middleName: String?,
        val lastName: String,
)
