package com.home.practice.app.dto.http.account

import java.util.*

data class AccountDto(
        val id: UUID,
        val username: String,
        val email: String,
        val firstName: String,
        val middleName: String?,
        val lastName: String,
)
