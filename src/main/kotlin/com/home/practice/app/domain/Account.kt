package com.home.practice.app.domain

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "accounts")
class Account(
        @Column(name = "username")
        var username: String,

        @Column(name = "email")
        var email: String,

        @Column(name = "first_name")
        var firstName: String,

        @Column(name = "middle_name")
        var middleName: String?,

        @Column(name = "last_name")
        var lastName: String

) : BaseEntity()
