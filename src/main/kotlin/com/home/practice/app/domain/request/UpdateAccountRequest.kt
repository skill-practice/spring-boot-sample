package com.home.practice.app.domain.request

data class UpdateAccountRequest(
        val username: String,
        val email: String,
        val firstName: String,
        val middleName: String?,
        val lastName: String,
)
