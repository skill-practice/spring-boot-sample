package com.home.practice.app.domain.request

data class AddAccountRequest(
        val username: String,
        val email: String,
        val firstName: String,
        val middleName: String?,
        val lastName: String,
)
