package com.home.practice.app.controller

import com.home.practice.app.dto.http.account.AccountDto
import com.home.practice.app.dto.http.account.AddAccountRequestDto
import com.home.practice.app.dto.http.account.UpdateAccountRequestDto
import com.home.practice.app.useCase.AccountUseCase
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/v1/accounts")
class AccountController(
        private val accountUseCase: AccountUseCase
) {

    @GetMapping("/{id}")
    fun get(
            @PathVariable id: UUID
    ): AccountDto {
        return accountUseCase.get(id)
    }

    @PostMapping
    fun add(
            @RequestBody requestDto: AddAccountRequestDto
    ): AccountDto {
        return accountUseCase.add(requestDto)
    }

    @PutMapping("/{id}")
    fun update(
            @PathVariable id: UUID,
            @RequestBody requestDto: UpdateAccountRequestDto
    ): AccountDto {
        return accountUseCase.update(id, requestDto)
    }

    @DeleteMapping("/{id}")
    fun delete(
            @PathVariable id: UUID
    ) {
        accountUseCase.delete(id)
    }

}