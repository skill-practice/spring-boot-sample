package com.home.practice.app.exception

open class ApplicationException(
        override val message: String?,
        override val cause: Throwable? = null
) : Exception(message)
