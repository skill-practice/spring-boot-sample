package com.home.practice.app.exception

import kotlin.reflect.KClass

open class NotFoundException(
        override val message: String?
) : ApplicationException(message) {
    constructor(
            objectType: KClass<*>,
            attribute: Any,
            attributeType: String = "id"
    ) : this("${objectType.simpleName} couldn't be found with $attributeType: '$attribute'")
}
