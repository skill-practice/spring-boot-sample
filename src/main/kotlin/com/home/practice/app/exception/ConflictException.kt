package com.home.practice.app.exception

open class ConflictException(
        message: String?
) : ApplicationException(message)
