package com.home.practice.app.useCase

import com.home.practice.app.dto.http.account.AccountDto
import com.home.practice.app.dto.http.account.AddAccountRequestDto
import com.home.practice.app.dto.http.account.UpdateAccountRequestDto
import com.home.practice.app.mapper.AccountMapper
import com.home.practice.app.service.AccountService
import org.springframework.stereotype.Service
import java.util.*

interface AccountUseCase {
    fun get(id: UUID): AccountDto
    fun add(requestDto: AddAccountRequestDto): AccountDto
    fun update(id: UUID, requestDto: UpdateAccountRequestDto): AccountDto
    fun delete(id: UUID)
}

@Service
internal class AccountUseCaseImpl(
        private val accountService: AccountService,
        private val accountMapper: AccountMapper
) : AccountUseCase {

    override fun get(id: UUID): AccountDto {
        return accountService
                .get(id)
                .let { accountMapper.toDto(it) }
    }

    override fun add(requestDto: AddAccountRequestDto): AccountDto {
        val request = accountMapper.toAddRequest(requestDto)

        return accountService
                .add(request)
                .let { accountMapper.toDto(it) }
    }

    override fun update(
            id: UUID,
            requestDto: UpdateAccountRequestDto
    ): AccountDto {
        val request = accountMapper.toUpdateRequest(requestDto)
        val account = accountService.get(id)

        return accountService
                .update(account, request)
                .let { accountMapper.toDto(it) }
    }

    override fun delete(id: UUID) {
        accountService.delete(id)
    }

}
