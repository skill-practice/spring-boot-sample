package com.home.practice.app.mapper

import com.home.practice.app.domain.Account
import com.home.practice.app.domain.request.AddAccountRequest
import com.home.practice.app.domain.request.UpdateAccountRequest
import com.home.practice.app.dto.http.account.AccountDto
import com.home.practice.app.dto.http.account.AddAccountRequestDto
import com.home.practice.app.dto.http.account.UpdateAccountRequestDto
import org.springframework.stereotype.Service

interface AccountMapper {
    fun toDto(entity: Account): AccountDto
    fun toAddRequest(requestDto: AddAccountRequestDto): AddAccountRequest
    fun toUpdateRequest(requestDto: UpdateAccountRequestDto): UpdateAccountRequest
}

@Service
internal class AccountMapperImpl : AccountMapper {

    override fun toDto(entity: Account): AccountDto {
        return AccountDto(
                id = entity.id!!,
                username = entity.username,
                email = entity.email,
                firstName = entity.firstName,
                middleName = entity.middleName,
                lastName = entity.lastName,
        )
    }

    override fun toAddRequest(requestDto: AddAccountRequestDto): AddAccountRequest {
        return AddAccountRequest(
                username = requestDto.username,
                email = requestDto.email,
                firstName = requestDto.firstName,
                middleName = requestDto.middleName,
                lastName = requestDto.lastName,
        )
    }

    override fun toUpdateRequest(requestDto: UpdateAccountRequestDto): UpdateAccountRequest {
        return UpdateAccountRequest(
                username = requestDto.username,
                email = requestDto.email,
                firstName = requestDto.firstName,
                middleName = requestDto.middleName,
                lastName = requestDto.lastName,
        )
    }

}
