package com.home.practice.app.repository

import com.home.practice.app.domain.Account
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface AccountRepository : JpaRepository<Account, UUID> {
    fun findByUsername(username: String): Account?

}
