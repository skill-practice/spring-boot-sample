package com.home.practice.app.service

import com.home.practice.app.domain.Account
import com.home.practice.app.domain.request.AddAccountRequest
import com.home.practice.app.domain.request.UpdateAccountRequest
import com.home.practice.app.exception.NotFoundException
import com.home.practice.app.repository.AccountRepository
import org.springframework.stereotype.Service
import java.util.*

interface AccountService {
    fun get(id: UUID): Account
    fun findByUsername(username: String): Account?
    fun add(request: AddAccountRequest): Account
    fun update(entity: Account, request: UpdateAccountRequest): Account
    fun delete(id: UUID)
}

@Service
internal class AccountServiceImpl(
        private val accountRepository: AccountRepository
) : AccountService {

    override fun get(id: UUID): Account {
        return accountRepository
                .findById(id)
                .orElseThrow { NotFoundException(Account::class, id, "id") }
    }

    override fun findByUsername(username: String): Account? {
        return accountRepository.findByUsername(username)
    }

    override fun add(request: AddAccountRequest): Account {
        val account = Account(
                username = request.username,
                firstName = request.firstName,
                middleName = request.middleName,
                lastName = request.lastName,
                email = request.email
        )

        return accountRepository.save(account)
    }

    override fun update(
            entity: Account,
            request: UpdateAccountRequest
    ): Account {
        return entity.apply {
            this.username = request.username
            this.firstName = request.firstName
            this.middleName = request.middleName
            this.lastName = request.lastName

            accountRepository.save(this)
        }
    }

    override fun delete(id: UUID) {
        accountRepository.deleteById(id)
    }

}
