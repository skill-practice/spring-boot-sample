CREATE TABLE accounts
(
    id          VARCHAR(36) PRIMARY KEY,
    username    VARCHAR(64)  NOT NULL UNIQUE,
    email       VARCHAR(100) NOT NULL UNIQUE,
    first_name  VARCHAR(64)  NOT NULL,
    middle_name VARCHAR(64),
    last_name   VARCHAR(64)  NOT NULL
);
