package com.home.practice.app.data

import com.home.practice.app.domain.Account
import com.home.practice.app.dto.http.account.AccountDto
import java.util.*

object AccountData {
    fun getAccount(
            username: String = "username1",
            email: String = "username1@mail.com",
            firstName: String = "Jack",
            middleName: String? = "Francis",
            lastName: String = "Jones",
    ): Account {
        return Account(
                username = username,
                email = email,
                firstName = firstName,
                middleName = middleName,
                lastName = lastName,
        )
    }

    fun getAccountDto(
            id: UUID = UUID.randomUUID(),
            username: String = "username1",
            email: String = "username1@mail.com",
            firstName: String = "Jack",
            middleName: String? = "Francis",
            lastName: String = "Jones",
    ): AccountDto {
        return AccountDto(
                id = id,
                username = username,
                email = email,
                firstName = firstName,
                middleName = middleName,
                lastName = lastName,
        )
    }
}