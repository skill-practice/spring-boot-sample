package com.home.practice.app.controller

import com.home.practice.app.domain.Account
import com.home.practice.app.repository.AccountRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles

@ActiveProfiles("test")
@SpringBootTest
class AccountControllerTest {

    @Autowired
    lateinit var accountRepository: AccountRepository

    @Autowired
    lateinit var accountController: AccountController

    @Test
    fun get_shouldReturnAccount(){
        // GIVEN
        val account = Account(
                username = "jack_jones",
                email = "jack_jones@mail.com",
                firstName = "Jack",
                middleName = "Miles",
                lastName = "Jones",
        )
        accountRepository.save(account)

        // WHEN
        val result = accountController.get(account.id!!)

        // THEN
        assertEquals(account.username, result.username)
        assertEquals(account.email, result.email)
        assertEquals(account.firstName, result.firstName)
        assertEquals(account.middleName, result.middleName)
        assertEquals(account.lastName, result.lastName)
    }

}