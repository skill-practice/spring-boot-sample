package com.home.practice.app.repository

import com.home.practice.app.data.AccountData
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import java.util.*

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class AccountRepositoryTest {

    @Autowired
    lateinit var accountRepository: AccountRepository

    @Test
    fun findByUsername_shouldReturnUser() {
        // GIVEN
        val username = "max123"
        val email = "max123@mail.com"
        val account = AccountData.getAccount(
                username = username,
                email = email
        )
        accountRepository.save(account)

        // WHEN
        val result = accountRepository.findByUsername(username)

        // THEN
        assertNotNull(result)
        assertEquals(username, result!!.username)
    }

    @Test
    fun findByUsername_shouldReturnNull() {
        // GIVEN
        val username: String = UUID.randomUUID().toString()

        // WHEN
        val result = accountRepository.findByUsername(username)

        // THEN
        assertNull(result)
    }
}