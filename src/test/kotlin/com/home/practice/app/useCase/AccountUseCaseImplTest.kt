package com.home.practice.app.useCase

import com.home.practice.app.data.AccountData
import com.home.practice.app.domain.Account
import com.home.practice.app.dto.http.account.AccountDto
import com.home.practice.app.mapper.AccountMapper
import com.home.practice.app.service.AccountService
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import java.util.*

@ExtendWith(MockitoExtension::class)
internal class AccountUseCaseImplTest {

    @Mock
    lateinit var accountService: AccountService

    @Mock
    lateinit var accountMapper: AccountMapper

    @InjectMocks
    lateinit var accountUseCaseImpl: AccountUseCaseImpl

    @Test
    fun get_shouldReturnAccount() {
        // GIVEN
        val id: UUID = UUID.randomUUID()
        val username = "user123"
        val email = "user123@mail.com"
        val account: Account = AccountData.getAccount(
                username = username,
                email = email
        )
        val accountDto: AccountDto = AccountData.getAccountDto(
                username = username,
                email = email
        )

        // WHEN
        Mockito.`when`(accountService.get(id)).thenReturn(account)
        Mockito.`when`(accountMapper.toDto(account)).thenReturn(accountDto)

        val result = accountUseCaseImpl.get(id)

        // THEN
        assertEquals(accountDto.id, result.id)
    }
}