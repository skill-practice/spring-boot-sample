package com.home.practice.app.api

import com.home.practice.app.UserApplication
import com.home.practice.app.domain.Account
import com.home.practice.app.dto.http.account.AccountDto
import com.home.practice.app.repository.AccountRepository
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.test.context.ActiveProfiles
import java.lang.String.format
import java.net.URI
import java.util.*

@ActiveProfiles("test")
@SpringBootTest(
        classes = [UserApplication::class],
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
class AccountControllerApiTest(
) {

    @Autowired
    lateinit var accountRepository: AccountRepository

    @Autowired
    lateinit var restTemplate: TestRestTemplate

    @LocalServerPort
    var port: Int = 0

    @Test
    fun get_shouldReturnAccount() {
        // GIVEN
        val account = Account(
                username = "mike_anderson",
                email = "147852369asdzxc",
                firstName = "Mike",
                middleName = "Miles",
                lastName = "Anderson",
        )
        accountRepository.save(account)
        val url = format(ACCOUNT_RESOURCE_URL, port, account.id)

        // WHEN
        val result: ResponseEntity<AccountDto> = restTemplate.getForEntity(URI(url), AccountDto::class.java)

        // THEN
        assertNotNull(result.body)

        val resultAccount = result.body!!

        assertEquals(account.username, resultAccount.username)
        assertEquals(account.email, resultAccount.email)
        assertEquals(account.firstName, resultAccount.firstName)
        assertEquals(account.middleName, resultAccount.middleName)
        assertEquals(account.lastName, resultAccount.lastName)
    }

    @Test
    fun get_shouldReturnNotFound() {
        // GIVEN
        val url = format(ACCOUNT_RESOURCE_URL, port, UUID.randomUUID())

        // WHEN
        val result: ResponseEntity<Void> = restTemplate.getForEntity(URI(url), Void::class.java)

        // THEN
        assertEquals(HttpStatus.NOT_FOUND, result.statusCode)
        assertNull(result.body)
    }

    companion object {
        private const val ACCOUNT_URL = "http://localhost:%s/api/v1/accounts"
        private const val ACCOUNT_RESOURCE_URL = "$ACCOUNT_URL/%s"
    }
}